package com.nesterov;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        TaxService taxService = new TaxService(); // Создание объекта налоговой службы.
        Supermarket supermarket1 = new Supermarket(taxService, 5); // Создание объекта супермаркета №1.
        Supermarket supermarket2 = new Supermarket(taxService, 5); // Создание объекта супермаркета №2.
        Supermarket supermarket3 = new Supermarket(taxService, 5); // Создание объекта супермаркета №3.

        ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()); // Создание исполнителя с фиксированным пулом потоков.
        service.submit(supermarket1); // Передача задачи исполнителю для выполнения.
        service.submit(supermarket2); // Передача задачи исполнителю для выполнения.
        service.submit(supermarket3); // Передача задачи исполнителю для выполнения.
        service.awaitTermination(3, TimeUnit.SECONDS); // Ожидание завершения всех задач в течение 3 секунд.

        System.out.println("Общая выручка: " + taxService.getAdder().sum()); // Вывод общей выручки на экран.
        service.shutdown(); // Остановка исполнителя.
    }
}