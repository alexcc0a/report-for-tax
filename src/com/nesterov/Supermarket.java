package com.nesterov;

import java.util.ArrayList;
import java.util.List;

public class Supermarket extends Thread { // Объявление класса Supermarket, который расширяет класс Thread.
    private List<Integer> bills; // Объявление списка чеков.
    private int maxValue; // Объявление максимальной суммы чеков.
    private TaxService taxService; // Объявление налоговой службы.

    public Supermarket(TaxService taxService, int maxValue) { // Конструктор класса Supermarket.
        bills = new ArrayList<>(); // Инициализация списка чеков.
        this.maxValue = maxValue; // Установка максимальной суммы чеков.
        this.taxService = taxService; // Установка налоговой службы.
        fillListBill(); // Вызов метода для заполнения списка чеков.
    }

    // Геттеры и сеттеры для полей класса.
    public TaxService getTaxService() {
        return taxService;
    }

    public void setTaxService(TaxService taxService) {
        this.taxService = taxService;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public List<Integer> getBills() {
        return bills;
    }

    public void setBills(List<Integer> bills) {
        this.bills = bills;
    }

    public void fillListBill() { // Метод для заполнения списка чеков.
        for (int i = 0; i < maxValue; i++) { // Цикл для генерации случайных чеков.
            bills.add((int) (Math.random() * ((maxValue + 1)))); // Добавление случайного чека в список.
        }
        System.out.println(bills); // Вывод списка чеков на экран.
    }

    @Override
    public void run() { // Переопределение метода run для выполнения задачи в отдельном потоке.
        for (Integer integer : bills) { // Цикл для обработки каждого чека из списка.
            taxService.getAdder().add(integer); // Добавление суммы чека к общей выручке в налоговой службе.
        }
    }
}
