package com.nesterov;

import java.util.concurrent.atomic.LongAdder;

// Класс налоговой службы.
public class TaxService { // Объявление класса TaxService.
    private LongAdder adder; // Объявление атомарного счетчика.

    public TaxService() { // Конструктор класса TaxService.
        adder = new LongAdder(); // Инициализация атомарного счетчика.
    }

    // Геттеры и сеттеры для поля adder.
    public LongAdder getAdder() {
        return adder;
    }

    public void setAdder(LongAdder adder) {
        this.adder = adder;
    }
}